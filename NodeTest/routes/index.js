//String prototype modifications
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

var _ = require('underscore');
var os = require('os');
var express = require('express');
var router = express.Router();

var fltspcService = require("fltspcService");
var mongo = require('mongo-service');
var similarity = require('accel-compute-similarity/similarity');
var ws = require("nodejs-websocket");

var USERS = {
    razi: {
        fltspcSurfaceId: "542130011c8231143700010d",
        widgetID: null
    },
    kwec: {
        fltspcSurfaceId: "547db0278adb7e755c000405",
        widgetID: null
    },
    test: {
        fltspcSurfaceId: "memdex",
        widgetID: null
    }
};
var AVAILABILITY_STATES = {
    green: {
        message: "Available - come right in...",
        color: "green"
    },
    yellow: {
        message: "Available for urgent business - please knock...",
        color: "yellow"
    },
    red: {
        message: "Unavailable - please do not disturb...",
        color: "red"
    },
    gone: {
        message: "Not in - will be back in around:",
        color: "red"
    }
}
var GESTURES = {
    full_slow: AVAILABILITY_STATES.yellow,
    full_fast: AVAILABILITY_STATES.red,
    full_slow_noclose: AVAILABILITY_STATES.green,
    slow_close_from_open: AVAILABILITY_STATES.green,
    fast_close_from_open: AVAILABILITY_STATES.red
}

// Find current IP Address
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}

console.log(addresses);
if(addresses. length !== 1){
    throw new Error("cant determine IP address")
}

var ipAddress = _.first(addresses);
//var ipAddress = '212.60.123.139';
var TESSEL_CONTENT_URL = '{"url":"http://{0}:3000/getui/{1}"}';

var mongoService = new mongo();

var webSocketServer = ws.createServer(function (conn) {
    console.log("New connection");

    conn.on("text", function (str) {
        console.log("Received "+str);
    });

    conn.on("close", function (code, reason) {
        console.log("Connection closed");
    });
}).listen(8081)

function broadcast(server, msg) {
    if(server.connections.length == 0){
        console.log("Broadcast: No connections available...");
    }
    server.connections.forEach(function (conn) {
        conn.sendText(msg)
    })
}

router.post('/tesselready', function(req, res) {

    // Don't create fltspc widget if in training mode
    if(req.body.mode === 'T'){
        res.end("done");
        return;
    }

    var user = req.body.user;
    var surfaceID = USERS[user].fltspcSurfaceId;
    var widgetID = USERS[user].widgetID;
    // Set up fltspc with a widget on the appropriate surface
    fltspcService.setWidget(surfaceID, widgetID, TESSEL_CONTENT_URL.format(ipAddress, user), function onSuccess(newWidgetID){

        // A new widgetID is provided if we did not find any previously created
        // fltspc widget and had to create a new one
        if(newWidgetID){
            USERS[user].widgetID = newWidgetID;
        }

        res.end("done");
    });
});

var mongoReadings = null;
router.post('/tesseldata', function(req, res) {
    if(!mongoReadings){
        mongoService.getRecords(function onSuccess(data) {
            mongoReadings = data;
            classify(mongoReadings);
        });

    } else {
        classify(mongoReadings);
    }

    function classify(mongoReadings){
        console.log('classifying...');
        var user = req.body.user;
        console.log('read user');
        var reading = req.body.sensor_reading;
        console.log('read sensor...');
        var pressCount = req.body.press_count;
        console.log("read presscount");

        if (mongoService.isDataOk(reading)) {
            console.log('data is OK');

            // Execution timing start
            var start = new Date().getTime();

            var gesture;
            if(reading.length > 0){
                // pre-process data
                var processedReading = mongoService.processData(reading);
                var comparisons = _.map(mongoReadings, function (storedReading) {
                    return {
                        cost: similarity.computeSimilarity(processedReading, storedReading.data).totalCost,
                        "class": storedReading.classLabel
                    };
                });

                var decision = _.first(_.sortBy(comparisons, function (comparison) {
                    return comparison.cost;
                }));

                // Execution timing end
                var end = new Date().getTime();
                var time = end - start;
                console.log('Execution time: ' + time +' ms');
                console.log("decision:", decision.class);
                console.log("cost:", decision.cost);
                gesture = GESTURES[decision.class];
            }

            var msg = JSON.stringify({
                user: user,
                message: (gesture && gesture.message) || AVAILABILITY_STATES.gone.message,
                color: (gesture && gesture.color) || AVAILABILITY_STATES.gone.color,
                time: pressCount * 600
            });

            broadcast(webSocketServer, msg);

        }else{
            console.log("data not ok (too few readings)");
        }
    }
    res.end("done");
});

router.param('userid', function(req, res, next, name) {
    // do validation on name here
    // blah blah validation
    // log something so we know its working
    console.log('doing name validations on ' + name);

    // once validation is done save the new item in the req
    req.name = name;
    // go to the next thing
    next();
});

/* GET home page. */
router.get('/getui/:userid', function(req, res) {
    res.render('index', {
        title: 'Pervasive Project Experiment',
        owner: req.params.userid,
        time: 0,
        message: "No activity yet"
    });
    res.end("done");
});

var processedData = null;
router.post('/mongodb/assign_class', function(req, res) {
    // await this call after receiving good data
    if (processedData) {
        console.log('assigning class');
        console.log(processedData);
        // get classification label from request (should implement input validation!)
        processedData = { classLabel: req.body.classLabel, data: processedData };
        mongoService.insertRecord(processedData, function() {
            console.log('data insert successful');
            processedData = null;
        });
    } else {
        throw new Error('processedData is null');
    }
    res.end();
});

router.post('/mongodb/clear_gforce', function(req, res) {
    console.log('clear called');
   mongoService.clearDb(function onSuccess() {
       console.log('cleared collection');
       res.end();
   });
});

router.get('/mongodb/gforce', function(req, res) {
    mongoService.getRecords(function onSuccess(data) {
        res.end(JSON.stringify(data));
    });
});

/*
    From postman call:
    Assign class: POST http://10.6.6.119:3000/mongodb/assign_class - body -> {"classLabel":"slowpoke"}
    Clear DB: POST http://10.6.6.119:3000/mongodb/clear_gforce
    Get all records: GET http://10.6.6.119:3000/mongodb/gforce
 */
router.post('/mongodb/gforce', function(req, res) {
    // check if data is ok - min 20 readings
    var data = req.body.sensor_reading;
    console.log('checking data');
    if (mongoService.isDataOk(data)) {
        console.log('data is OK');
        // pre-process data
        processedData = mongoService.processData(data);
    }else{
        console.log("data not ok (too few readings)");
    }
    res.end("done");
});

module.exports = router;
