/**
 * Created by kapper on 19/11/14.
 */
var tessel = require('tessel');
var accel = require('accel-mma84').use(tessel.port['B']);

var _startX = 0.01;
var _startY = 1.01;
var _startZ = 0.0095;


// Initialize the accelerometer.
accel.on('ready', function () {
    // Stream accelerometer data
    accel.on('data', function (xyz) {
        console.log(
//            'x:', xyz[0].toFixed(10),
//            'y:', xyz[1].toFixed(10),
            _startZ.toFixed(5),
            'z:', xyz[2].toFixed(5),
            Math.abs(_startZ - Math.abs(xyz[2])),
            isMoving(xyz[0], xyz[1], xyz[2]
            )
        );
    });

});

function isMoving(x,y,z) {
//    console.log(_startY);
//    console.log(y);
//    var isMovingX = Math.abs(_startX - Math.abs(x)) > 0.05;
//    console.log(Math.abs(_startX - Number(x)));
//    var isMovingY = Math.abs(_startY - Math.abs(y)) > 0.05;
//    console.log(Math.abs(_startY - Math.abs(y)));
    var isMovingZ = Math.abs(_startZ - Math.abs(z)) > 0.005;
//    console.log(Math.abs(_startZ - Number(z)));
    if (/*isMovingX || isMovingY ||*/ isMovingZ) {
        return true;
    }
    return false;
}

accel.on('error', function(err){
    console.log('Error:', err);
});