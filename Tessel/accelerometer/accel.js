// ______________________________________ Prototype modifications _________________________________________________

if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

// ______________________________________ Dependencies _________________________________________________

var wifi = require('wifi-cc3000');
var tessel = require('tessel');
var http = require('http');
var _ = require('underscore');
var pin = tessel.port['GPIO'].pin['G3'];

// ______________________________________ Enums _________________________________________________

var _MODES = {
    Training: "T",
    Live: "L"
}

// ______________________________________ Globals _________________________________________________

var _USER_ID = process.argv[2] || "razi";
var _MODE = process.argv[3] || _MODES.Live;
//var _SERVER_IP = process.argv[4] || "212.60.123.248";
var _SERVER_IP = process.argv[4] || "192.168.43.102";
var _startX;
var _startY;
var _startZ;
var _tempArrX = [];
var _tempArrY = [];
var _tempArrZ = [];
var _isSending = false;
var _readingButtonInput = false;
var _retries = 0;
var _MAX_RETRIES = 1;
var _array = [];
var _notMoving = 0;
var _hasMoved = false;
var _press_count = 0;
var _release_count = 0;

var led1 = tessel.led[0].output(1);
var led2 = tessel.led[1].output(0);

var DefaultHttpOptions = {
    hostname: _SERVER_IP,
    port: '3000',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
};

// ______________________________________ Program _________________________________________________

start();

function start() {

    // startup on wifi connect (onConnect emission is bugged)
    setTimeout(function check() {
        if (wifi.isConnected()) {
            console.log('Live mode detected');
            if (_MODE === _MODES.Live) {
                announceReady();
            } else {
                console.log('Training mode detected');
                initAccelerometer();
            }
        } else {
            setTimeout(check, 100);
        }
    }, 0);
}

function announceReady(){
    announce(function onSuccess() {
        setTimeout(function() {
            initButton();
            initAccelerometer();
        }, 5000);
    });
}
function determineStartValues (x, y, z){
    console.log("calculating starting values...");
    _tempArrX.push(x);
    _tempArrY.push(y);
    _tempArrZ.push(z);

    console.log(_tempArrZ.length);
    if(_tempArrZ.length === 15){
         var totalX = _.reduce(_tempArrX, function (memo, num) {
            return memo + num;
        }, 0)
        _startX = totalX/_tempArrX.length;

        var totalY = _.reduce(_tempArrY, function (memo, num) {
            return memo + num;
        }, 0)
        _startY = totalY/_tempArrY.length;

        var totalZ = _.reduce(_tempArrZ, function (memo, num) {
            return memo + num;
        }, 0)
        _startZ = totalZ/_tempArrZ.length;
        _tempArrX.length = 0;
        _tempArrY.length = 0;
        _tempArrZ.length = 0;
        console.log("startX:", _startX, "startY", _startY, "startZ", _startZ);
    }
}

function resetStartValues(){
    _startX = null;
    _startY = null;
    _startZ = null;
}

function initButton() {
    console.log('initializing pressure sensor');
    setTimeout(function readButton() {
        var pinReading = pin.read();
        console.log("pin reading:", pinReading, "presscount:", _press_count, "releasecount:", _release_count);
        if (pinReading === 1) {
            _press_count++
        }
        setTimeout(readButton, 100);
    }, 0)
}

function initAccelerometer(){
    console.log('initializing accelerometer');

    // blinking debug
    setInterval(function () {
        if(_isSending){
            led2.write(0);
            led1.toggle();
        } else {
            led1.write(0);
            led2.toggle();
        }
    }, 100);

    var accel = require('accel-mma84').use(tessel.port['B']);
    accel.on('ready', function () {
        console.log('Accelerometer ready');

        accel.on('data', function (xyz) {
            var x = xyz[0];
            var y = xyz[1];
            var z = xyz[2];

            if(!_startX || !_startY || !_startZ){
                determineStartValues(x, y, z);
            }else if(_startX && _startY && _startZ && !_isSending && !_readingButtonInput){
                if(isMoving(x,y,z)){
                    _hasMoved = true;
                    _notMoving = 0;
                }else{
                    _notMoving++;
                }

                console.log("hasMoved:", _hasMoved, "notMoving:", _notMoving, "arraySize:", _array.length);
                // While moving
                if (_hasMoved) {
                    _array.push({x:x, y:y, z:z});
                    if(_array.length >= 200){
                        _array.length = 0;      // clear array
                        _notMoving = 0;
                        _hasMoved = false;
                        resetStartValues();
                    }
                }

                if (_notMoving >= 20 && _hasMoved) {
                    console.log('Pushing accelerometer data');
                    console.log("data.length:",_array.length);
                    _array.splice(_array.length-20, 20);
                    console.log("data.length after splice:",_array.length);

                    if (_press_count === 0) {

                    }

                    var data = {
                        readings: _press_count ? [] : _array,
                        press_count: _press_count
                    };

                    sendData(data, resetValuesAfterSend);
                }
            }
        });
    });

    accel.on('error', function(err){
        console.log('Accel error:', err);
    });
}

function resetValuesAfterSend() {
    _readingButtonInput = false;
    _isSending = false;
    _retries = 0;
    _array.length = 0;      // clear array
    _notMoving = 0;
    _hasMoved = false;
    _press_count = 0;
}

function isMoving(x,y,z) {
    var diffX = Math.abs(Math.abs(_startX) - (Math.abs(x)));
    var diffY = Math.abs(Math.abs(_startY) - (Math.abs(y)));
    var diffZ = Math.abs(Math.abs(_startZ) - (Math.abs(z)));
    var isMovingX = diffX > 0.05;
    var isMovingY = diffY > 0.05;
    var isMovingZ = diffZ > 0.02;
    console.log(isMovingX,isMovingY,isMovingZ);
    if (isMovingX || isMovingY || isMovingZ) {
        return true;
    }
    return false;
}

function announce(callback) {
    _isSending = true;

    var announceHttpOptions = {
        path: '/tesselready'
    }

    var httpOptions = _.extend(DefaultHttpOptions, announceHttpOptions);
//    console.log(httpOptions);
    console.log('announcing...');
    var req = http.request(httpOptions, function(res) {
        res.setEncoding('utf8');
        res.on('data', function () {
            console.log('Response received.');
        });

        res.on('end', function(){
            _isSending = false;
            _retries = 0;
            callback();
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ', e.message);

        // on error loop again
        console.log("Retrying...");
        if(_retries <= _MAX_RETRIES){
            setTimeout(function(){
                _retries++;
                start();
            }, 5000);
        }else{
            console.log("All retries failed. Attempting connection reset");
            setTimeout(function(){
                if(wifi.isConnected()){
                    wifi.disconnect();
                    console.log("Wifi disconnected");
                }
                _retries = 0;
                setTimeout(function(){
                    start();
                }, 5000)
            }, 1000);
        }
    });

    req.write(JSON.stringify({
        user: _USER_ID,
        mode: _MODE
    }));
    req.end();
    console.log('Announce request for {0} sent on {1}..'.format(_USER_ID, httpOptions.path));
};

function sendData(data, callback) {
    _isSending = true;

    var sendDataHttpOptions = {
        path: _MODE === _MODES.Training ? '/mongodb/gforce' : '/tesseldata'
    }

    var httpOptions = _.extend(DefaultHttpOptions, sendDataHttpOptions);

    var req = http.request(httpOptions, function(res) {
        res.setEncoding('utf8');
        res.on('data', function () {
            console.log('Response received.');
        });

        res.on('end', function(){
            callback();
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ', e.message);

        // on error loop again
        console.log("Retrying...");
        if(_retries <= _MAX_RETRIES){
            setTimeout(function(){
                _retries++;
                sendData(data, callback);
            }, 5000);
        }else{
            console.log("All retries failed. Attempting connection reset");
            setTimeout(function(){
                if(wifi.isConnected()){
                    wifi.disconnect();
                    console.log("Wifi disconnected");
                }
                _retries = 0;
                setTimeout(function(){
                    wifi.connect();
                    sendData(data, callback);
                }, 5000)
            }, 1000);
        }
    });
    req.write(JSON.stringify({
        sensor_reading: data.readings,
        press_count: data.press_count,
        user: _USER_ID
    }));

    req.end();
    console.log('Posted {0} readings and {1} presses on {2}'.format(data.readings.length, data.press_count, httpOptions.path));
};